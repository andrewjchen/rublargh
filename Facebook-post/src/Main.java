import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.types.FacebookType;
import com.restfb.types.Page;
import com.restfb.types.User;


public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// DefaultFacebookClient is the FacebookClient implementation
		// that ships with RestFB. You can customize it by passing in
		// custom JsonMapper and WebRequestor implementations, or simply
		// write your own FacebookClient instead for maximum control.
		
		//get token here:https://developers.facebook.com/tools/explorer
		String access_token = "your token here";

		FacebookClient facebookClient = new DefaultFacebookClient(access_token);
		
		User user = facebookClient.fetchObject("me", User.class);
		Page page = facebookClient.fetchObject("cocacola", Page.class);

		System.out.println("User name: " + user.getName());
		System.out.println("Page likes: " + page.getLikes());
		
		
		FacebookType publishMessageResponse =
				  facebookClient.publish("me/feed", FacebookType.class,
				    Parameter.with("message", "Facebook's GraphAPI is great! I think everybody should own one!"));

		System.out.println("Published message ID: " + publishMessageResponse.getId());

	}

}
