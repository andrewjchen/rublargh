/*---------------------------------------------------------------------------------

---------------------------------------------------------------------------------*/
#include <nds.h>
#include <dswifi9.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include <stdio.h>
#include <string.h>

#include "rc4.h"

void getHttp(char* url);
	
//---------------------------------------------------------------------------------
int main(void) {
//---------------------------------------------------------------------------------

	consoleDemoInit();  //setup the sub screen for printing

	iprintf("\n\n\tSimple Wifi Connection Demo\n\n");
	iprintf("Connecting via WFC data ...\n");

	if(!Wifi_InitDefault(WFC_CONNECT)) {
		iprintf("Failed to connect!");
	} else {

		iprintf("Connected\n\n");


        int listensock;
        listensock = socket(AF_INET, SOCK_STREAM, 0);
        iprintf("Created socket: %08X\n", listensock);

        struct sockaddr_in sain;
        sain.sin_family = AF_INET;
        sain.sin_port = htons(12345);
        sain.sin_addr.s_addr = INADDR_ANY;

        int ret;
        ret = bind(listensock, &sain, sizeof(sain));
        iprintf("bind returned %d\n", ret);

        ret = listen(listensock, 1);
        iprintf("listen returned %d\n", ret);

        struct sockaddr_in client_sain;
        int addrlen = sizeof(client_sain);
        int clientsock = accept(listensock, &client_sain, &addrlen);
        iprintf("client socket %d\n");

        struct rc4_state rc4s;
        rc4_init(&rc4s, "hunter2", 7);

        while(1)
        {
            unsigned char c;
            unsigned char buf[256];
            ret = recv(clientsock, &c, 1, 0);
            iprintf("Got length byte %d (ret = %d)\n", c, ret);
            if(ret != 1)
                while(1);
            ret = recv(clientsock, buf, c, 0);
            iprintf("Got %d bytes!\n", ret);
            if(ret != c)
                while(1);
            rc4_crypt(&rc4s, buf, buf, c);
            ret = send(clientsock, buf, c, 0);
            iprintf("Wrote %d bytes!\n", ret);
        }

		//getHttp("www.akkit.org");	}
    }
	
	
	while(1) {
		swiWaitForVBlank();
	}

	return 0;
}

//---------------------------------------------------------------------------------
void getHttp(char* url) {
//---------------------------------------------------------------------------------
    // Let's send a simple HTTP request to a server and print the results!

    // store the HTTP request for later
    const char * request_text = 
        "GET /dswifi/example1.php HTTP/1.1\r\n"
        "Host: www.akkit.org\r\n"
        "User-Agent: Nintendo DS\r\n\r\n";

    // Find the IP address of the server, with gethostbyname
    struct hostent * myhost = gethostbyname( url );
    iprintf("Found IP Address!\n");
 
    // Create a TCP socket
    int my_socket;
    my_socket = socket( AF_INET, SOCK_STREAM, 0 );
    iprintf("Created Socket!\n");

    // Tell the socket to connect to the IP address we found, on port 80 (HTTP)
    struct sockaddr_in sain;
    sain.sin_family = AF_INET;
    sain.sin_port = htons(80);
    sain.sin_addr.s_addr= *( (unsigned long *)(myhost->h_addr_list[0]) );
    connect( my_socket,(struct sockaddr *)&sain, sizeof(sain) );
    iprintf("Connected to server!\n");

    // send our request
    send( my_socket, request_text, strlen(request_text), 0 );
    iprintf("Sent our request!\n");

    // Print incoming data
    iprintf("Printing incoming data:\n");

    int recvd_len;
    char incoming_buffer[256];

    while( ( recvd_len = recv( my_socket, incoming_buffer, 255, 0 ) ) != 0 ) { // if recv returns 0, the socket has been closed.
        if(recvd_len>0) { // data was received!
            incoming_buffer[recvd_len] = 0; // null-terminate
            iprintf(incoming_buffer);
		}
	}

	iprintf("Other side closed connection!");

	shutdown(my_socket,0); // good practice to shutdown the socket.

	closesocket(my_socket); // remove the socket.
}

