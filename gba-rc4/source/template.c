
#include <gba_console.h>
#include <gba_video.h>
#include <gba_interrupt.h>
#include <gba_systemcalls.h>
#include <gba_input.h>
#include <gba_sio.h>
#include <stdio.h>
#include <stdlib.h>
#include "rc4.h"

//---------------------------------------------------------------------------------
// Program entry point
//---------------------------------------------------------------------------------
int main(void) {
//---------------------------------------------------------------------------------


	// the vblank interrupt must be enabled for VBlankIntrWait() to work
	// since the default dispatcher handles the bios flags no vblank handler
	// is required
	irqInit();
	irqEnable(IRQ_VBLANK);

	consoleDemoInit();

	// ansi escape sequence to set print co-ordinates
	// /x1b[line;columnH
	//iprintf("\x1b[10;10HHello World!\n");

	REG_RCNT = 0;
	REG_SIOCNT = SIO_115200 | (1 << 7) | (1 << 10) | (1 << 11) | SIO_UART;
	REG_SIOCNT = SIO_115200 | (1 << 7) | (1 << 8) | (1 << 10) | (1 << 11) | SIO_UART;

	while (1) {
		if((REG_SIOCNT & (1 << 5)) == 0)
		{
			unsigned char c = REG_SIODATA8;
			iprintf("Got data %02X\n", c);

			while((REG_SIOCNT & (1 << 4)));

			REG_SIODATA8 = c;
		}
	}
}


